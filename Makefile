CC = g++
CCFLAGs = -Wall -Wextra -Werror -g
OBJECTs = TADPiece.o TADBoard.o main.o

all: tateti
%.o: %.c
	$(CC) $(CCFLAGS) -c -o $@ $^

tateti: $(OBJECTS)
	$(CC) $(CCFLAGS) -o $@ $^

start: tateti
	./tateti

run_valgrind: tateti
	valgrind --show-reachable=yes --leak-check=full ./tateti

debug: tateti
	gdb ./tateti

clean:
	rm -f tateti $(OBJECTS)